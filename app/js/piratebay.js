function Methods() {

    if(typeof window._methods !== 'undefined'){
        return window._methods;
    }

    var Methods = {
        processing_post: false,
        processing_block : true,
        actions : {},
        protection : false,
        preloaded : [],
        ADD_ACTION : function(name, callback){
            Methods.actions[name] = callback;
        },
        CALL_ACTION : function(name, context, data){
            if(name in Methods.actions){
                Methods.actions[name].apply(context, data);
            }
        },
        INIT_EVENTS : function(){
            $(document).on('click', "a[href='#']", function (e) {
                e.preventDefault();
                e.stopPropagation();
            }).on('click', '[data-action]', function (e) {
                e.preventDefault();
                e.stopPropagation();
                Methods.CALL_ACTION($(this).data('action'), this);
            }).on('change', 'input[data-action]', function(){
                Methods.CALL_ACTION($(this).data('action'), this);
            }).on('change', 'select[data-action]', function(){
                Methods.CALL_ACTION($(this).data('action'), this);
            }).on('click', '[data-cp]', function(e){
                e.preventDefault();
                var data = $(this).data();
                if(data.cp){
                    data.message = data.message || 'Copied to clipboard!';
                    var $container = $('<input>');
                    //$('body').append($container);
                    $(this).after($container);
                    $container.val(data.cp).select();
                    document.execCommand("copy");
                    $container.remove();
                    Methods.SHOW_TOASTR(data.message, 'success');
                }
            }).on('keyup', 'input[data-actionkey]',function(){
                Methods.CALL_ACTION($(this).data('actionkey'), this);
            }).on('click touchend', '[data-href]', function(e){
                e.preventDefault();
                window.location.href = $(this).data('href');
            }).on('keyup', 'input[data-submit]', function(e){
                if(e.which == 13) {
                    e.preventDefault();
                    var action = $(this).data('submit');
                    $('[data-action='+action+']').trigger('click');
                }
            });
        },
        SHOW_MODAL : function(content, __callback, _class){
            var $container = $('#autoModals');
            _class = _class || '';
            if($container.length === 0){
                $container = $('<div>').attr('id', 'autoModals');
                $('body').append($container);
            }
            var $modal = $('<div>').addClass('modal in').attr('tabindex', -1).attr('role', 'dialog').attr('aria-labelledby', 'myModalLabel').append($('<div>').addClass('modal-dialog ' + _class).append($('<div>').addClass('modal-content'))).hide();
            $modal.find('.modal-content').append(content);
            $container.empty().append($modal);
            if(__callback){
                $modal.on('hide.bs.modal', function(){
                    __callback.apply(this);
                });
            }
            //Methods.CLOSE_MODAL();
            $($modal).modal();
        },
        GET_MODAL : function(){
            var $container = $('#autoModals');
            return $container.children('.modal');
        },
        CLOSE_MODAL : function(){
            if ($(document).find('.modal').length) {
                $(document).find('.modal').modal('hide');
            }
            // fix если открыты последовательно несколько модалок
            $('body').children('.modal-backdrop').stop(1,1).fadeOut(250, function(){
                $('body').removeClass('modal-open');
                $(this).remove();
            });
        },
        SCROLL_TO : function(selector, offset, speed) {
            var $element = $(selector);
            var speed = speed || 600;
            if ($element.length) {
                $('html,body').animate({scrollTop: $element.offset().top - offset}, speed);
            }
        },
        preload : function(files){
            $(files).each(function(){
                Methods.preloaded[this] = $('<img/>').attr('src', this);
            });
        }
    };

    Methods.INIT_EVENTS();

    window._methods = Methods;

    return Methods;

}(jQuery);

$(function(){

    function shuffle(a) {
        var j, x, i;
        for (i = a.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = a[i];
            a[i] = a[j];
            a[j] = x;
        }
        return a;
    }

    var $gameContainer = $('#gameContainer');
    var baseUrl = $('meta[name="burl"]').attr('content');

    var ENVIRONMENT = {

        $mp3modal : $('#play_mp3'),

        isSafari : function() {
            return /^((?!chrome|android).)*safari/i.test(navigator.userAgent.toLowerCase());
        },
        mobileAndTabletcheck : function() {
            var check = false;
            (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
            return check;
        },
        music : {
            enabled: true,
            sounds: {
                bg: {file: baseUrl + 'assets/sounds/pb/bg_normal.mp3', obj: null},
                bg_play: {file: baseUrl + 'assets/sounds/pb/bg_play.mp3', obj: null},
                play: {file: baseUrl + 'assets/sounds/pb/start.mp3?1', obj: null},
                minus: {file: baseUrl + 'assets/sounds/pb/btn_multiplier.mp3', obj: null},
                plus: {file: baseUrl + 'assets/sounds/pb/btn_multiplier.mp3', obj: null},
                coin_1: {file: baseUrl + 'assets/sounds/pb/coin_1.mp3', obj: null},
                coin_2: {file: baseUrl + 'assets/sounds/pb/coin_2.mp3', obj: null},
                coin_3: {file: baseUrl + 'assets/sounds/pb/coin_3.mp3', obj: null},
                coin_4: {file: baseUrl + 'assets/sounds/pb/coin_4.mp3', obj: null},
                coin_5: {file: baseUrl + 'assets/sounds/pb/chest.mp3', obj: null},
                lost: {file: baseUrl + 'assets/sounds/pb/bones.mp3', obj: null},
                win: {file: baseUrl + 'assets/sounds/pb/coin_4.mp3', obj: null},
                anchor: {file: baseUrl + 'assets/sounds/pb/anchor.mp3?1', obj: null},
                withdraw: {file: baseUrl + 'assets/sounds/pb/take_prize.mp3', obj: null},
                pop_3: {file: baseUrl + 'assets/sounds/pb/pop_yohoho.mp3', obj: null},
                pop_4: {file: baseUrl + 'assets/sounds/pb/pop_epic.mp3', obj: null},
                pop_5: {file: baseUrl + 'assets/sounds/pb/pop_big.mp3', obj: null}

            },
            audio_enabled: function() {
                return (typeof window.Audio === "function" && this.enabled);
            },
            play: function(obj) {
                if (this.audio_enabled()) {
                    if (typeof obj === "object" && typeof(obj.file) !== 'undefined' && typeof(obj.obj) !== 'undefined') {
                        if (obj.obj == null) { // если объект пустой инициализируем
                            obj.obj = new window.Audio(obj.file);
                        }
                        obj.obj.play(); // играем
                        return true;
                    }
                }
                return false;
            },
            pause: function(obj) {

                if (typeof obj == "object" && typeof(obj.file) != "undefined" && typeof(obj.obj) != "undefined") {
                    if (obj.obj != null) { // если объект не пустой
                        obj.obj.currentTime = 0; // отматываем на начало
                        obj.obj.pause(); // останавливаем
                        return true;
                    }
                }

            },
            loop: function(obj) {
                if (this.audio_enabled()) {
                    if (obj.obj != null) {
                        obj.obj.addEventListener('ended', function(){ // хендлер достижения окончания проигрывания объекта
                            if (ENVIRONMENT.music.audio_enabled()) {
                                obj.obj.currentTime = 0; // перематываем на начало
                                obj.obj.play(); // играем
                            }
                        });
                        return true;
                    }
                }
                return false;
            }

        },

        play_or_ask : function() {
                this.music.play(this.music.sounds.bg); // играем
                this.music.loop(this.music.sounds.bg); // вешаем хендлер на окончание проигрывания и перезапуск
        }
    };


    /** @constructor */
    var ROUND = function(index){

        var thisRound = this;

        this.index = index;
        this.index_choice = null;
        this.is_open = false;
        this.selector = ['[data-round="', this.index, '"]'].join('');
        this.$element = $gameContainer.find(this.selector);
        this.$draw_result = this.$element.find('.draw_result');
        this.choices = {
            0 : this.$element.find('[data-choice="0"]'),
            1 : this.$element.find('[data-choice="1"]'),
            2 : this.$element.find('[data-choice="2"]'),
        };
        this.data = this.$element.data();

        // current round choice
        this.$element.on('click', '[data-choice]', function(){
            var data = $(this).data();
            thisRound.choice(data.choice);
        });

        return this;
    };

    ROUND.prototype = {
        initial : function(){
            this.is_open = false;
            this.index_choice = null;
            this.$element.removeClass('active complete won');
            this.$element.find('.bet').removeClass('won lost chest anchor a-flip');
            // сброс в дефолт
            this.calc = Math.round(GAME.data.bet * GAME.data.multi * GAME.data.float * GAME.data.x * this.data.rwon) / GAME.data.float;
            this.amount = this.calc;
            this.setAmount(this.calc, false);
        },
        open : function(){
            this.is_open = true;
            this.$element.addClass('active');
            // открытие строки
        },
        choice : function(choiceIndex){
            if(this.is_open && this.index_choice === null && choiceIndex in this.choices){
                this.index_choice = choiceIndex;
                this.choices[this.index_choice].addClass('a-buzz');
                GAME.sendRequest(GAME.method_choise, {choice : choiceIndex, round : this.index});
            }
        },
        setAmount : function(amount, istext){
            this.amount = amount;
            amount = $('<span>').html(amount);
            if(!istext){
                this.$draw_result.empty().append(amount).append(GAME.getCurrencySpan());
            } else {
                this.$draw_result.empty().append(amount);
            }
        },
        getAmount : function(){
            return this.amount;
        },
        setWon : function(){
            this.$element.addClass('complete won');
            if(this.index_choice !== null){
                this.choices[this.index_choice].removeClass('a-buzz').addClass('won a-flip');
            }
            return this;
        },
        setAnchor : function(){
            this.$element.addClass('complete');
            if(this.index_choice !== null){
                this.choices[this.index_choice].removeClass('a-buzz').addClass('anchor a-flip');
                this.calc = Math.round(GAME.data.bet * GAME.data.multi * GAME.data.float * GAME.data.x * this.data.rlow) / GAME.data.float;
                this.setAmount(this.calc, false);
            }
            return this;
        },
        setLost : function(){
            if(this.index_choice !== null){
                this.choices[this.index_choice].removeClass('a-buzz').addClass('lost a-flip');
                this.setAmount('Try again', true);
            }
            return this;
        },
        setClass : function(index, classname){
            if(index in this.choices){
                this.choices[index].removeClass('a-buzz').addClass(classname);
            }
        }
    };

    var GAME = {

        data : $gameContainer.data(),
        devmode : true,

        method_play : '/buytickets?method=play',
        method_choise : '/buytickets?method=choice',
        method_withdraw : '/buytickets?method=withdraw',
        method_endgame : '/buytickets?method=endgame',
        method_finder_search : '/finder/search',
        method_finder_gamedata : '/finder/gameData',

        bet : false,
        betHash : false,
        betRoundIndex : 0,
        betProcessing : false,
        active : false,
        update_inprogress : false,
        update_interval : 5000,
        update_hash : false,
        update_instance : false,

        buttons : {
            $play : $('#playGame'),
            $withdraw : $('#withdrawGame'),
            $endGame : $('#endGame')
        },

        $balance : $('#pb_balance'),
        $betAmount : $('#betAmount'),
        $currency : $('#pb_currency'),
        $recentTable : $('#recentTable'),

        choice_class : {0:'lost', 1:'anchor', 2:'won'},
        allow_multi : {1:1, 2:2, 3:3, 4:4, 5:5, 6:10, 7:50, 8:100},
        allow_currency : {1:'ɃTC', 2:'mɃTC', 3: 'ƀits'},
        allow_currency_class : {1:'i-btc', 2:'i-mbtc', 3: 'i-bits'},
        allow_currency_multi : {1:1, 2:1000, 3: 1000000},
        allow_currency_float : {1:1000000, 2:1000, 3: 1},

        rounds : {
            1 : new ROUND(1),
            2 : new ROUND(2),
            3 : new ROUND(3),
            4 : new ROUND(4),
            5 : new ROUND(5)
        },

        start : function(){
            if(!this.active){
                this.update_interval = 5000;
                this.active = true;
                this.bet = false;
                this.betHash = false;
                $.each(this.rounds, function(){
                    this.initial();
                });
                $gameContainer.removeClass('a-lucky');
                this.animate.showEnd();
                this.sounds.play();
                this.console('Start game!');
                return true;
            }
            return false;
        },
        stop : function(){
            this.betRoundIndex = 0;
            this.active = false;
            this.closeAllRounds();
            this.animate.blockedWithdraw(true);
            this.animate.showPlay();
            this.console('Stop game!');
            return true;
        },
        console : function(event, extra){
            if(this.devmode){
                console.log(event, this.betHash, extra);
            }
        },

        // здесь меняем визуально анимацию событий
        animate : {
            showPlay : function(){
                GAME.buttons.$withdraw.hide();
                GAME.buttons.$endGame.hide();
                GAME.buttons.$play.fadeIn(100);
                this.blockedPlay(false);
            },
            showWithdraw : function(withBalance){
                if(withBalance){
                    var element = {
                        calc : Math.round(GAME.bet.bet_amount * GAME.data.multi * GAME.data.float * GAME.getCurrentRound().data.rwon ) / GAME.data.float,
                        $text : $('<span>').text(GAME.buttons.$withdraw.data('text') + ' ')
                    };
                    element.$price = $('<span>').addClass('take-price').text(element.calc);
                    GAME.buttons.$withdraw.empty().append(element.$text).append(element.$price).append(GAME.getCurrencySpan());
                }
                GAME.buttons.$play.hide();
                GAME.buttons.$endGame.hide();
                GAME.buttons.$withdraw.fadeIn(100);
                this.blockedWithdraw(false);
            },
            showEnd : function(){
                GAME.buttons.$play.hide();
                GAME.buttons.$withdraw.hide();
                GAME.buttons.$endGame.fadeIn(100);
                this.blockedPlay(false);
            },
            blockedPlay : function(blocked){
                blocked = blocked || false;
                GAME.buttons.$play.toggleClass('disabled', blocked);
            },
            blockedWithdraw : function(blocked){
                blocked = blocked || false;
                GAME.buttons.$withdraw.toggleClass('disabled', blocked);
            },
            gameLost : function(){
                this.blockedPlay(true);
                this.blockedWithdraw(true);
            },
            gameWithdraw : function(){
                this.blockedPlay(false);
                this.blockedWithdraw(true);
                //var calc = Math.round(GAME.bet.bet_balance * GAME.data.multi * GAME.data.float ) / GAME.data.float;
            },
            gameWonHigh : function(){
                GAME.showWinBox(GAME.betRoundIndex, GAME.getCurrentRound().getAmount());
            },
            gameWonTotal : function(){
                $gameContainer.addClass('a-lucky');
                GAME.showWinBox(5, GAME.getRound(5).getAmount());
            },
            gameWonLow : function(){

            }
        },

        sounds : {
            play : function(){
                ENVIRONMENT.music.pause(ENVIRONMENT.music.sounds.bg);
                ENVIRONMENT.music.play(ENVIRONMENT.music.sounds.bg_play);
                ENVIRONMENT.music.loop(ENVIRONMENT.music.sounds.bg_play);
                return ENVIRONMENT.music.play(ENVIRONMENT.music.sounds.play);
            },
            plus : function(){
                return ENVIRONMENT.music.play(ENVIRONMENT.music.sounds.plus);
            },
            minus : function(){
                return this.plus();
            },
            coin : function(index){
                if(index in ENVIRONMENT.music.sounds){
                    return ENVIRONMENT.music.play(ENVIRONMENT.music.sounds[index]);
                }
            },
            win : function(){
                return ENVIRONMENT.music.play(ENVIRONMENT.music.sounds.win);
            },
            lost : function(){
                return ENVIRONMENT.music.play(ENVIRONMENT.music.sounds.lost);
            },
            withdraw : function(){
                return ENVIRONMENT.music.play(ENVIRONMENT.music.sounds.withdraw);
            },
            anchor : function(){
                return ENVIRONMENT.music.play(ENVIRONMENT.music.sounds.anchor);
            },
            winbox : function(index){
                return ENVIRONMENT.music.play(ENVIRONMENT.music.sounds['pop_' + index]);
            }
        },

        getRound : function(index){
            index = parseInt(index);
            if(index in this.rounds){
                return this.rounds[index];
            }
            return false;
        },

        getCurrentRound : function(){
            return this.getRound(this.betRoundIndex);
        },

        setAmount : function(amount){
            amount = $('<span>').html(amount);
            this.$betAmount.empty().append(amount).append(this.getCurrencySpan());
        },

        setBalance : function(amount){
            this.$balance.data('balance', amount);
            this.recalcBalance();
        },

        recalcBalance : function(){
            var balance = parseFloat(this.$balance.data('balance'));
            this.calcBalance = Math.round(balance * this.data.multi * this.data.float) / this.data.float;
            this.$balance.text(this.calcBalance);
        },

        closeActiveRounds : function(){
            $.each(this.rounds, function(){
                if(this.$element.hasClass('active')){
                    this.is_open = false;
                    this.$element.removeClass('active');
                }
            });
        },

        closeAllRounds : function(){
            $.each(this.rounds, function(){
                this.is_open = false;
                this.$element.removeClass('active');
            });
        },

        getCurrencySpan : function(asText){
            if(this.data.currency in this.allow_currency_class){
                if(asText === true){
                    return this.allow_currency[this.data.currency];
                }
                return $('<span>').addClass(this.allow_currency_class[this.data.currency]);
            }
            return 'unk';
        },

        onPlayComplete : function(response){
            this.bet = {
                bet_amount : GAME.data.x * 0.00002,
                bet_balance : 0,
                boardData : {
                    1 : false,
                    2 : false,
                    3 : false,
                    4 : false,
                    5 : false,
                },
                code : 0
            };
            this.data.bet = response.bet_price;
            this.recalcAmounts();
            this.getRound(1).open();
            this.betRoundIndex = 1;
            this.animate.blockedPlay(true);
            this.console('Event: onPlayComplete', this.bet);
        },

        onWonHigh : function(response){
            // open next round
            response.nextRound = parseInt(response.round) + 1;
            this.getCurrentRound().setWon();
            this.closeActiveRounds();
            if(response.nextRound > 5) { // bigwin
                this.animate.gameWonTotal();
                this.sounds.coin('coin_' + 5);
                this.animate.blockedWithdraw(true);
                this.console('Event: onWonHigh. FINAL!', this.bet);
                this.stop();
            } else {
                this.animate.gameWonHigh();
                this.sounds.coin('coin_' + this.betRoundIndex);
                this.animate.showWithdraw(true);
                this.betRoundIndex = response.nextRound;
                this.getCurrentRound().open();
                this.console('Event: onWonHigh', this.bet);
            }
        },

        onWonLow : function(){
            this.getCurrentRound().setAnchor();
            this.animate.gameWonLow();
            this.sounds.anchor();
            this.console('Event: onWonLow', this.bet);
            // stop the game
            this.stop();
        },

        onLost : function(){
            this.getCurrentRound().setLost();
            // show lost animation
            this.animate.gameLost();
            this.sounds.lost();
            this.console('Event: onLost', this.bet);
            // and stop the game
            this.stop();
        },

        onWithdraw : function(){
            this.animate.gameWithdraw();
            this.sounds.withdraw();
            this.console('Event: onWithdraw', this.bet);
            // and stop the game
            this.stop();
        },

        onEndGame : function(){
            this.console('Event: onEndGame', this.bet);
            // and stop the game
            this.stop();
        },

        onShowFinal : function(){
            var round = false;
            var numbers = [];
            var cl = '';
            if(this.bet.boardData){
                $.each(this.bet.boardData, function(iRound, dataRound){
                    round = GAME.getRound(iRound);
                    numbers = dataRound.numbers;
                    if(round){
                        $.each(round.choices, function(i, el){
                            if(round.index_choice !== i && numbers[i] in GAME.choice_class){
                                if(numbers[i] == 2){
                                    cl = GAME.choice_class[numbers[i]];
                                    round.setClass(i, cl);
                                }
                            }
                        });
                    }
                });
            };
            this.console('Show final board', this.bet);
        },

        sendRequest : function(url, data){
            data = data || {};
            data.hash = GAME.betHash;
            data.round = GAME.betRoundIndex;
            this.console('Send request', data);
            switch (url) {
                case GAME.method_play: return Methods().CALL_ACTION('play', this, [data]);
                case GAME.method_choise: return Methods().CALL_ACTION('choice', this, [data]);
                case GAME.method_withdraw: return Methods().CALL_ACTION('withdraw', this, [data]);
                case GAME.method_endgame: return Methods().CALL_ACTION('endgame', this, [data]);
            }
        },

        recalcAmounts : function(){
            this.recalcBalance();
            this.calc = Math.round(this.data.bet * this.data.multi * this.data.float * this.data.x) / this.data.float;
            this.setAmount(this.calc);
            this.calcBalance = Math.round(this.data.bet * this.data.multi * this.data.float * this.data.x) / this.data.float;
            $.each(this.rounds, function(){
                this.calc = Math.round(GAME.data.bet * GAME.data.multi * GAME.data.float * GAME.data.x * this.data.rwon) / GAME.data.float;
                this.setAmount(this.calc, false);
            });
        },

        winbox : {
            3 : $('#winbox-win'),
            4 : $('#winbox-bigwin'),
            5 : $('#winbox-epic')
        },
        overlay : {
            $container : $('#win-overlay'),
            $box : false
        },
        showWinBox : function(round, amount){
            this.overlay.$container.hide();
            if(round in this.winbox){
                this.overlay.$box = this.winbox[round];
                this.overlay.$container.find('._winbox_amount').html(amount + ' ' + GAME.getCurrencySpan(true));
                $.each(this.winbox, function(){
                    $(this).hide();
                });
                this.overlay.$box.show();
                setTimeout(function(){
                    GAME.overlay.$container.fadeIn(100);
                    GAME.sounds.winbox(round);
                }, 50);
            }
        },
        hideWinBox : function(){
            this.overlay.$container.fadeOut(100);
        }

    };
    Methods().processing_block = false;

    window.PirateBay = GAME;

    Methods().ADD_ACTION('switchOn',function() {
        ENVIRONMENT.music.play(ENVIRONMENT.music.sounds.bg);
        ENVIRONMENT.music.loop(ENVIRONMENT.music.sounds.bg);
        // $('#play_mp3').modal('hide');
    });

    Methods().ADD_ACTION('switchSound', function() {

        ENVIRONMENT.music.enabled = !ENVIRONMENT.music.enabled;


        if ($(this).hasClass('sound-on')) {
            ENVIRONMENT.music.pause(ENVIRONMENT.music.sounds.bg);
            ENVIRONMENT.music.pause(ENVIRONMENT.music.sounds.bg_play);
            $(this).removeClass('sound-on').addClass('sound-off');
        }else {
            if (PirateBay.active) {
                ENVIRONMENT.music.play(ENVIRONMENT.music.sounds.bg_play);
                ENVIRONMENT.music.loop(ENVIRONMENT.music.sounds.bg_play);
            }else {
                ENVIRONMENT.music.play(ENVIRONMENT.music.sounds.bg);
                ENVIRONMENT.music.loop(ENVIRONMENT.music.sounds.bg);
            }

            $(this).removeClass('sound-off').addClass('sound-on');
        }


    });

    Methods().ADD_ACTION('playGame', function(){
        if(GAME.start()){
            GAME.sendRequest(GAME.method_play, {multiplier : GAME.data.x});
        }
    });

    Methods().ADD_ACTION('withdrawGame', function(){
        if(GAME.active){
            GAME.sendRequest(GAME.method_withdraw);
        }
    });

    Methods().ADD_ACTION('endGame', function(){
        if(GAME.active){
            GAME.sendRequest(GAME.method_endgame);
        }
    });

    Methods().ADD_ACTION('setMin', function(){
        if(!GAME.active){
            if(GAME.data.x > 1){
                GAME.data.xid = 1;
                GAME.data.x = GAME.allow_multi[1];
                GAME.recalcAmounts();
                GAME.sounds.minus();
            }
        }
    });

    Methods().ADD_ACTION('setMinus', function(){
        if(!GAME.active){
            GAME.data.xid--;
            if(GAME.data.xid in GAME.allow_multi){
                GAME.data.x = GAME.allow_multi[GAME.data.xid];
                GAME.recalcAmounts();
                GAME.sounds.minus();
            } else {
                GAME.data.xid = 1;
            }
        }
    });

    Methods().ADD_ACTION('setPlus', function(){
        if(!GAME.active){
            GAME.data.xid++;
            if(GAME.data.xid in GAME.allow_multi){
                GAME.data.x = GAME.allow_multi[GAME.data.xid];
                GAME.recalcAmounts();
                GAME.sounds.plus();
            } else {
                GAME.data.xid = 8;
            }
        }
    });

    Methods().ADD_ACTION('setMax', function(){
        if(!GAME.active){
            if(GAME.data.x < 8){
                GAME.data.xid = 8;
                GAME.data.x = GAME.allow_multi[8];
                GAME.recalcAmounts();
                GAME.sounds.plus();
            }
        }
    });

    Methods().ADD_ACTION('closeModal', function(){
        var data = $(this).data();
        if(data.selector){
            $(data.selector).fadeOut(100);
        }
    });

    Methods().ADD_ACTION('openModal', function(){
        var data = $(this).data();
        if(data.selector){
            $(data.selector).fadeIn(100);
        }
    });

    Methods().ADD_ACTION('show_body', function(){
        var _ = $(this);
        var data = _.data();
        if(data.container){
            _.parent().find('.active').removeClass('active');
            $(data.container).closest('table').find('tbody').hide();
            $(data.container).fadeIn(100);
            _.addClass('active');
            $('[data-action="showmore"]').hide();
            $(data.container + '_btn').fadeIn(100);
        }
    });

    Methods().ADD_ACTION('close_overlay', function(){
        GAME.hideWinBox();
    });

    $('.styled').styler();

    $gameContainer.on('change', GAME.$currency, function(){
        GAME.data.currency = parseInt(GAME.$currency.val());
        if(GAME.data.currency in GAME.allow_currency_multi){
            GAME.data.multi = GAME.allow_currency_multi[GAME.data.currency];
            GAME.data.float = GAME.allow_currency_float[GAME.data.currency];
            //$('#pb_balance_cur').empty().append(GAME.getCurrencySpan());
            GAME.recalcAmounts();
        }
    });

    Methods().preload([
        baseUrl + '/assets/img/pb_wins_sprite.png'
    ]);

    // фнк показывает финальный результат (генерит недостающие результаты раундов)
    function showFinal(){
        $.each(GAME.bet.boardData, function(iRound, iRoundData){
            if(!iRoundData){
                GAME.bet.boardData[iRound] = {numbers : shuffle([0,1,2])};
            }
        });
        return GAME.onShowFinal();
    };

    // нажатие на кнопке play
    Methods().ADD_ACTION('play', function(data){
        var response = {
            bet_price : 0.00002,
            code : 1,
            round : 1,
        };
        return GAME.onPlayComplete(response);
    });

    // выбор ячейки. в момент выбора генерируется результат раунда
    Methods().ADD_ACTION('choice', function(data){
        var results = [2,2,0];
        // if(!introJSAlreadyStarted) {
        //     results = data.round === 1 ? [2,2,2] : [0,0,0];
        // }
        var response = {
            bet : {},
            bet_price : 0.00002,
            choice : data.choice,
            code : 1,
            round : data.round,
            results : results,
            choiceResults : false
        };
        shuffle(response.results); // рандомный результат
        GAME.bet.boardData[data.round] = {numbers : response.results};
        response.choiceResults = response.results[data.choice];
        // если выбор упал на ячейку с монетой
        if(response.choiceResults === 2){
            // if(!introJSAlreadyStarted) {
            //     PirateBayIntro.goToStep(2);
            //     setTimeout(function () {
            //         PirateBayIntro.goToStep(data.round + 2);
            //         setTimeout(function () {
            //             $('.tooltip-3').height(340);
            //             $('.tooltip-3+.introjs-tooltipReferenceLayer .introjs-tooltip').append('<div class="or-take-prize-text">Or<br><strong>Take</strong> your prize <div class="introjs-arrow bottom" style="display: inherit;"></div></div>');
            //             $('.tooltip-3+.introjs-tooltipReferenceLayer').height(340);
            //             $('.tooltip-3+.introjs-tooltipReferenceLayer .introjs-tooltip').css('transition', 'none').css('bottom', '315px').css('transform', 'translateY(0)');
            //             $('#withdrawGame').css('z-index', '9999999');
            //             if($(window).innerWidth() <= 768)
            //                 $('html,body').scrollTop(250);
            //         }, 1000)
            //     }, 1500);
            // }
            return GAME.onWonHigh(response);
        }
        // иначе игра заканчивается - показываем всю доску
        showFinal();

        // if(!introJSAlreadyStarted) {
        //     $('.tooltip-3+.introjs-tooltipReferenceLayer .introjs-tooltip .or-take-prize-text').hide();
        //     // PirateBayIntro.goToStep(7);
        //     $('html, body').scrollTop(0);
        //     setTimeout(function () {
        //         showPlayRealGameBlock();
        //     }, 2000);
        // }
        if(response.choiceResults === 1){
            return GAME.onWonLow();
        }
        return GAME.onLost(response);
    });

    // нажатие кнопки вывода в процессе игры
    Methods().ADD_ACTION('withdraw', function(){
        showFinal();
        return GAME.onWithdraw();
    });

    // просто выход из игры сразу после ставки
    Methods().ADD_ACTION('endgame', function(){
        showFinal();
        return GAME.onEndGame();
    });

    var $pageHeader = $('.page-header');

    function fixHeader() {
        if (window.scrollY > 100)
            $pageHeader.addClass('active');
        else
            $pageHeader.removeClass('active');
    }

    fixHeader();

    $(window).on("scroll", function () {
        fixHeader();
    });

    $('.open-sign-up-modal-btn').click(function () {
        trhModal.open('.registration-modal');
    });

    var $registrationForm = $('#registrationForm');
    $('.registration-modal .modal-close-btn').click(function () {
        $registrationForm.show();
        $registrationForm.next().hide();
    });

    $registrationForm.submit(function (e) {
        this.reset();
        $(this).hide();
        $(this).next().show();
    });

    $('#showMainSection').click(function () {
        $('.game_main_section').addClass('open');
        $(this).hide('slow');
    });

    $('#gameRegistrationForm').submit(function (e) {
        e.preventDefault();
        $('.game-form').hide();
        $('.game_draws').show();
        $('#playGame').show();
        window.open('https://pb.trueflip.io', '_blank').blur();
        window.focus();
        setTimeout(function () {
            $('#playGame').click();
        }, 1000);
    });

    $('.play-real-game-btn').click(function (e) {
        e.preventDefault();
        $('.game-play-real-game').hide();
        $('#gameRegistrationForm').show();
    });

    // var introJSAlreadyStarted = false;
    $('#playGame').click(function () {
        $('.game-form').hide();
        $('.game_draws').show();
        // if(!introJSAlreadyStarted) {
        //     //intro.start();
        //     $('body').addClass('introJs-active');
        // }
    });

    $('.play-demo-game-btn').click(function (e) {
        e.preventDefault();
        $('#playGame').click();
    });

    // $('.game_draws [data-round="1"] .bet').click(function () {
    //     if(!introJSAlreadyStarted) {
    //         intro.nextStep();
    //     }
    // });



    function showPlayRealGameBlock() {
        // intro.exit();
        // $('body').removeClass('introJs-active');
        $('.game_draws').hide();
        $('#gameRegistrationForm').hide();
        $('.game-form').show();
        $('.game-play-real-game').show();
        // introJSAlreadyStarted = true;
    }

    $('#withdrawGame').click(function() {
        showPlayRealGameBlock();
    });

    // var intro = introJs();
    // window.PirateBayIntro = intro;

    // intro.setOptions({
    //     steps: [{
    //         intro: '<strong>Win up to 5 BTC</strong><br>Guess where the treasure is!',
    //         element: document.querySelector('.game_draws [data-round="1"]'),
    //         position: 'top'
    //     },
    //     {
    //         intro: '<strong>Well done!</strong>',
    //         element: document.querySelector('.game_draws [data-round="1"]'),
    //         tooltipClass: 'without-arrow',
    //         position: 'top',
    //     },
    //     {
    //         intro: 'Now you may <strong>go for adventure further</strong></span>',
    //         element: document.querySelector('.game_draws [data-round="2"]'),
    //         highlightClass: 'tooltip-3',
    //         position: 'top',
    //         scrollTo: 'tooltip'
    //     },
    //     {
    //         intro: 'or<br><strong>Take</strong> your prize',
    //         element: document.querySelector('.game_draws [data-round="3"]'),
    //         highlightClass: 'tooltip-4',
    //         position: 'top'

    //     },
    //     {
    //         intro: 'Now you may <strong>go for adventure further</strong><br>or<br><strong>Take</strong> your prize',
    //         element: document.querySelector('.game_draws [data-round="4"]'),
    //         highlightClass: 'tooltip-5',
    //         position: 'top'
    //     },
    //     {
    //         intro: 'Now you may <strong>go for adventure further</strong><br>or<br><strong>Take</strong> your prize',
    //         element: document.querySelector('.game_draws [data-round="5"]'),
    //         highlightClass: 'tooltip-6',
    //         position: 'top'
    //     },
    //     {
    //         intro: 'Everything’s fine!<br><strong>Try one more time!</strong>',
    //         element: document.querySelector('.game_draws [data-round="5"]'),
    //         highlightClass: 'tooltip-7',
    //         tooltipClass: 'without-arrow',
    //         position: 'top'
    //     }],
    //     showButtons: false,
    //     showBullets: false,
    //     showStepNumbers: false,
    //     scrollToElement: true,
    //     scrollTo: 'tooltip',
    //     exitOnOverlayClick: false
    // });

    // $(window).click(function() {
    //     ENVIRONMENT.play_or_ask();
    // });

    window.playPirateBayBg = function() {
      ENVIRONMENT.play_or_ask();
    }

    window.stopPirateBayBgMusic = function() {
      ENVIRONMENT.music.pause(ENVIRONMENT.music.sounds.bg);
      ENVIRONMENT.music.pause(ENVIRONMENT.music.sounds.bg_play);
    }

});
