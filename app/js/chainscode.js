


cc = {};
cc.fn = {
    winbox : {
        1 : $('#winbox-win'),
        2 : $('#winbox-bigwin'),
        3 : $('#winbox-epicwin')
    },
    overlay : {
        $container : $('#win-overlay'),
    },
    showBigWin : function(index, amount){
        if(cc.fn.overlay.$container.hasClass('visible')){
            cc.fn.overlay.$container.removeClass('visible');
            cc.fn.overlay.$container.hide();
        }
        $.each(cc.fn.winbox, function(){
            $(this).hide();
        });
        setTimeout(function(){
            cc.fn.winbox[index].show();
            cc.fn.overlay.$container.addClass('visible');
            cc.fn.overlay.$container.find('._winbox_amount').text(amount);
            cc.fn.overlay.$container.fadeIn(50);
            cc.fn.overlay.$container.find('.btn').off().on('click', function(){
                cc.fn.hideBigWin();
            });
        }, 50);
    },
    hideBigWin : function(){
        cc.fn.overlay.$container.removeClass('visible');
        cc.fn.overlay.$container.fadeOut(100);
    }
};




if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(searchElement, fromIndex) {
      var k;
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }
  
      var O = Object(this);
      var len = O.length >>> 0;
  
      if (len === 0) {
        return -1;
      }
      var n = +fromIndex || 0;
      if (Math.abs(n) === Infinity) {
        n = 0;
      }
      if (n >= len) {
        return -1;
      }
      k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
      while (k < len) {
        if (k in O && O[k] === searchElement) {
          return k;
        }
        k++;
      }
      return -1;
    };
}




var cc_ticket_price = 5;
var bet_list = [];
var myBalance = 300; // start value for the balance;

ccgame = {};


ccgame.prize_table = [
    {'repetitions': [2], 'pairs': 1, 'special':false, 'multiplier': 0.2}, // 0
    {'repetitions': [2,2], 'pairs': 2, 'special':false, 'multiplier': 0.5}, // 1
    {'repetitions': [3], 'pairs': 1, 'special':false, 'multiplier': 1}, // 2
    {'repetitions': [2,3], 'pairs': 2, 'special':false, 'multiplier': 3}, // 3
    {'repetitions': [2,2,2], 'pairs': 3, 'special':false, 'multiplier': 5}, // 4
    {'repetitions': [4], 'pairs': 1, 'special':false, 'multiplier': 5}, // 5
    {'repetitions': [2,4], 'pairs': 2, 'special':false, 'multiplier': 10}, // 6
    {'repetitions': [3,3], 'pairs': 2, 'special':false, 'multiplier': 20}, // 7
    {'repetitions': [5], 'pairs': 1, 'special':false, 'multiplier': 30}, // 8
    {'repetitions': [6], 'pairs': 1, 'special':true, 'multiplier': 2500}, // 9 // MAX PRIZE earlier to prevent next condition
    {'repetitions': [6], 'pairs': 1, 'special':false, 'multiplier': 100}, // 10
    
];

ccgame.current_draw = false;
ccgame.current_balance = false;
ccgame.last_combo = '';
ccgame.last_uuid = '';
ccgame.response = false;

ccgame.current_multiplier = 1;
ccgame.current_screen = 1;

ccgame.in_process = false;
ccgame.balance_format = 'BTC';

//  SOUNDS
ccgame.sound_tap = null;
ccgame.sound_bg = null;
ccgame.sound_won = null;

ccgame.sound_process = null;
ccgame.sounds_enabled = true;

ccgame.results_timeout = 5000;
ccgame.interval_handler = false;

ccgame.multipliers = [1,2,3,4,5,10,50,100];
ccgame.init = function(){
    set_watchers = function() {
        $(document).ready(function() {
            
            ccgame.fn.animate('wait_for_a_game'); // init wait animation
            ccgame.fn.show_screen(1); // show 1s screen

            // we need click event to play bg sound on mobiles and in Safari
            if ($(document).find('#play_mp3').length /*&& ccgame.fn.isSafari() == true*/) {
               // $('#play_mp3').modal('show'); // show modal - for safari
            }else {
                ccgame.fn.enter_the_game();
            }
            
            ccgame.fn.recalc_balance(myBalance); // recalc balance
            
            Methods().preload([ // preload images
                'assets/img/cc-ray-more.png',
                'assets/img/cc-stars.png',
                'assets/img/cc-winsprite.png'
            ]);
            
        }).on('click touchend','.btn_minus',function(e){ // minus hanler for multiplier
            e.preventDefault();
            ccgame.fn.multiplier_handler($(this),$(this).next('span').find('span').text(),'minus');
        }).on('click touchend','.btn_plus',function(e){ // plus handler for multiplier
            e.preventDefault();
            ccgame.fn.multiplier_handler($(this),$(this).prev('span').find('span').text(),'plus');
        }).on('keyup', function(e) {
            e.which = e.which || e.keyCode;
            if(e.which == 13) {
                ccgame.fn.play();
            }
        }).on('click touchend', '#sound_control', function(e) {
            e.preventDefault();
            ccgame.sounds_enabled = !ccgame.sounds_enabled;
            ccgame.fn.bg_sound.resetSound();
            $(this).toggleClass('off');
            $('.sound_lamp').toggleClass('off');
            $(this).blur();
        })
    };
    set_watchers();
    
};

ccgame.fn = {
    getCombo_prize: function(combo) {
        var repeats = ccgame.fn.find_repeats(combo).sort();
        var counts = ccgame.fn.find_repeats(combo,true);
        var multiplier = 0;
        ccgame.prize_table.some(function(el,i) {
            if (el.pairs == repeats.length) {
                if (!el.special) {
                    var valid = false;
                    var stop = 0;
                    for(var j = 0; j< el.pairs; j++) {
                        if (el.repetitions[j] == counts[repeats[j]]) {
                            valid = true;
                        }else {
                            stop++;
                        }
                    }
                    if (valid == true && stop == 0) {
                        multiplier = el.multiplier;
                        return true;
                    }
                }else {
                    if (counts[repeats[0]] == 6 && repeats[0] == 7) {
                        multiplier = el.multiplier;
                        return true;
                    }
                }
                
            }
        });
        return multiplier;
    },
    isSafari: function() {
        var ua = navigator.userAgent.toLowerCase(); 
        if (ua.indexOf('safari') != -1) { 
            if (ua.indexOf('chrome') > -1) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    },
    generateCombo: function() {
        var numbers = [];
        for (var i = 0; i<=5; i++) {
            numbers.push(genterateNumber());
        }
        var combo = numbers.join('-');
        return combo;
        function genterateNumber() {
            return Math.floor(Math.random() * (8));
        }
    },
    update_processing : false,
    
    bg_sound: {
        loop: function() {
            //setInterval(ccgame.fn.bg_sound.checkifStoped,300)
            if (ccgame.sound_bg != null) {
                ccgame.sound_bg.addEventListener('ended', function(){
                    ccgame.sound_bg.currentTime = 0;
                    ccgame.sound_bg.play();
                });
            }
        },
        resetSound: function() {
            if (ccgame.sound_bg != null) {
                if (ccgame.sounds_enabled == true) {
                    ccgame.sound_bg.currentTime = 0;
                    ccgame.sound_bg.play();
                }else {
                    ccgame.sound_bg.currentTime = 0;
                    ccgame.sound_bg.pause();
                }
            }
            if (ccgame.sound_process != null && ccgame.sound_process.currentTime > 0) {
                if (ccgame.sounds_enabled == true) {
                    ccgame.sound_process.currentTime = 0;
                    ccgame.sound_process.play();
                }else {
                    ccgame.sound_process.currentTime = 0;
                    ccgame.sound_process.pause();
                }
            }
        }
    },
    enter_the_game: function() {
        if (typeof Audio == "function") {
            if (ccgame.sound_bg == null) {
                ccgame.sound_bg = new Audio('assets/sounds/cc_bg_play.mp3');
                setTimeout(function() {
                    ccgame.sound_bg.play();
                    ccgame.fn.bg_sound.loop();
                },1000);
            }
            if (ccgame.sound_tap == null) {
                ccgame.sound_tap = new Audio('assets/sounds/btn_start.mp3');
            }
            if (ccgame.sound_process == null) {
                ccgame.sound_process = new Audio('assets/sounds/bet_in_process.mp3');
            }
            if (ccgame.sound_won == null) {
                ccgame.sound_won = new Audio('assets/sounds/won.mp3?1');
            }
        }
        // $('#play_mp3').modal('hide');
    },
    betlist : [],
    
    rs : false,
    tmp : false,
    get_html_result: function(combo) {
        var cmb = [];
        if (typeof combo == "string") {
            cmb = combo.split('-');
        }
        var highlight = ccgame.fn.find_repeats(cmb);
        var tmp = '<ul>';
        if (cmb.length == 6) {
            for(var i = 0; i <= 5; i++) {
                var cl = (highlight.indexOf(cmb[i]) != -1) ? 'active' : '';
                tmp += '<li class="' + cl + '">' + cmb[i] + '</li>';
            }
        }
        tmp += '</ul>';
        return tmp;
    },
    show_results: function(type, val) {

        if (typeof(val.combo) != "undefined") {
            ccgame.last_combo = val.combo; // в хэндлере плеера - проставится само с задержкой
        }
        if (ccgame.sound_process != null) {
            ccgame.sound_process.pause();
            ccgame.sound_process.currentTime = 0;
        }
        
        switch (type) {
            case 'won':
                ccgame.fn.show_screen(25);
                setTimeout(function(){
                    ccgame.last_combo = '';
                    ccgame.fn.show_screen(3);
                    var cmb = val.combo.split('-');
                    if (cmb.length == 6) {
                        var highlight = ccgame.fn.find_repeats(cmb);
                        if (highlight.length){
                            $('.digit_'+ highlight.join(', .digit_')).addClass('highlighted');
                        }
                        $('.combo_block').addClass('won_combo');
                    }
                    
                    var current_balance = ccgame.fn.convertBTC(myBalance);
                    var won_amount = parseFloat(val.won_amount);
                    var result_value = Math.round(ccgame.fn.convertInto(won_amount,current_balance.currency) * 100000) / 100000;
                    var X = (won_amount / (ccgame.current_multiplier * cc_ticket_price));
                    if (X >= 10 && X < 30) {
                        cc.fn.showBigWin(1, result_value + ' ' + current_balance.currency);
                    }
                    if (X >= 30 && X < 2000) {
                        cc.fn.showBigWin(2, result_value + ' ' + current_balance.currency);
                    }
                    if (X >= 2000) {
                        cc.fn.showBigWin(3, result_value + ' ' + current_balance.currency);
                    }
                    
                    if (ccgame.sound_won != null && ccgame.sounds_enabled == true) {
                        ccgame.sound_won.play();
                    }

                    $('.combo_won').find('strong').text(result_value + ' ' + current_balance.currency);
                    $('.prize_won').text('+'+result_value + ' ' + current_balance.currency).addClass('go');
                    $('.cc_balance').addClass('won_balance');

                    ccgame.fn.recalc_balance(myBalance + result_value);

                    setTimeout(function(){
                        $('.prize_won').text('').removeClass('go');
                        $('.cc_balance').removeClass('won_balance');
                    },1500);
                    ccgame.current_draw = null;
                    ccgame.in_process = false;
                    $('#play_btn').removeClass('disabled');
                    
                }, 4000);
            break;
            case 'lost': 
                setTimeout(function(){
                    $('#play_btn').removeClass('disabled');
                    ccgame.last_combo = '';
                    
                    ccgame.fn.clear_heightlight();
                    ccgame.fn.show_screen(4);
                    ccgame.current_draw = null;
                    ccgame.in_process = false;
                    
                },4000);
                
            break;
        }
    },
    find_won_lost: function(combo) {
        var ret = [];
        if (typeof(combo) == "object" && combo.length == 6) {
            var result = {
                "0" : 0,
                "1" : 0,
                "2" : 0,
                "3" : 0,
                "4" : 0,
                "5" : 0,
                "6" : 0,
                "7" : 0
            };
            for (var i = 1; i <= 6; i++) {
                result[combo[i-1]]++;
                if (result[combo[i-1]] > 1 && ret.indexOf(combo[i-1]) == -1 ) {
                    return true;
                }
            }
        }
        return false;
    },
    find_repeats: function(combo, rep) { // [1,2,3,4,5,6]
        var ret = [];
        if (typeof(combo) == "object" && combo.length == 6){
            var result = {
                "0" : 0,
                "1" : 0,
                "2" : 0,
                "3" : 0,
                "4" : 0,
                "5" : 0,
                "6" : 0,
                "7" : 0
            };
            for (var i = 1; i <= 6; i++) {
                result[combo[i-1]]++;
                if (result[combo[i-1]] > 1 && ret.indexOf(combo[i-1]) == -1) {
                    ret.push(combo[i-1]);
                }
            }
            if (typeof rep != "undefined"){
                return result;
            }
        }
        
        return ret;
    },
    tap: function() {
        if (ccgame.sound_tap != null && ccgame.sounds_enabled == true) {
            ccgame.sound_tap.play();
        }
    },
    finalCurrentBet : function(memoryBet){
        if(memoryBet && ccgame.current_draw !== null){
            ccgame.current_draw = null;
            if(memoryBet.won == 1){
                ccgame.fn.show_results('won', memoryBet);
            }
            if(memoryBet.won == 0){
                ccgame.fn.show_results('lost', memoryBet);
            }
            
        }
    },
    play: function() {
        $('#play_btn').blur();
        if (typeof(ccgame.current_draw) != "string" && ccgame.last_draws !== '' && ccgame.in_process == false) {
            ccgame.in_process = true;
            $('#play_btn').addClass('disabled');
            var multiplier = 1;
            if (typeof ccgame.current_multiplier != "undefined" && ccgame.multipliers.indexOf(ccgame.current_multiplier) > -1) {
                multiplier = ccgame.current_multiplier;
            }
            //animate
            ccgame.fn.clear_animation();
            ccgame.fn.show_screen(2);
            ccgame.fn.animate('play_game');
            if (ccgame.sound_process != null && ccgame.sounds_enabled == true && ccgame.sound_process.currentTime == 0) {
                ccgame.sound_process.play();
            }
            
            var bet_price = cc_ticket_price * multiplier;
            myBalance -= bet_price;
            
            ccgame.fn.recalc_balance(myBalance);
            ccgame.current_draw = "someuuid";
                        
            var cmbo = ccgame.fn.generateCombo();
            var won = Math.floor(ccgame.fn.find_won_lost(cmbo.split('-')));
            var bet = {
                'combo': cmbo,
                'won': won,
                'won_amount': ccgame.fn.getCombo_prize(cmbo.split('-')) * multiplier * cc_ticket_price
            };
                        
            ccgame.fn.finalCurrentBet(bet);
            
        }

    },
    recalc_balance: function(balance) {
        myBalance = balance;
        var formated = ccgame.fn.convertBTC(balance);
        if (formated.currency == 'mBTC') {
            formated.value = formated.value.toFixed(3);
        }
        $('.cc_balance').html(formated.value + ' ' + formated.currency);
        ccgame.balance_format = formated.currency;

        ccgame.fn.recalc_bet_price();
    },
    recalc_bet_price: function() {
        var current_balance = ccgame.fn.convertBTC(myBalance);

        
        var current_bet = Math.round((cc_ticket_price * ccgame.current_multiplier) * 1000000) / 1000000; /// BTC
        var result_value = Math.round(ccgame.fn.convertInto(current_bet,current_balance.currency) * 1000000) / 1000000;
        if (current_balance.currency == 'mBTC') {
            result_value = result_value.toFixed(3);
        }
        $('#bet_amount').html(result_value + ' ' + current_balance.currency);
    },
    convertInto: function(from,to_currency) {
        switch (to_currency) {
            case 'BTC': 
                return from;
            break;
            case 'mBTC':
                return from * 1000;
            break;
            case 'μBTC':
                return from * 1000000;
            break;
        }
        return from;
    },
    convertBTC: function(from) {
        var to = from;
        var currency = '$';
        /*if (from < 0.001) {
            to = from * 1000000;
            currency = 'μBTC';
        }else if(from >= 0.001 && from < 1) {
            to = from * 1000;
            currency = 'mBTC';
        }*/
        var rt = Math.round(to * 1000000) / 1000000;
        if (from < 0.001) {
            rt = rt.toFixed(3);
        }
        return { 'value' : rt, 'currency' : currency };
    },
    show_screen:function(number) {
        //$( ".game_glitch_wr" ).mgGlitch({destroy : true }); 
        $('.cc_step_1, .cc_step_2, .cc_step_3, .cc_step_4, .cc_step_5, .cc_step_25').removeClass('active_step');
        $('.cc_step_1, .cc_step_2, .cc_step_3, .cc_step_4, .cc_step_5, .cc_step_25').hide();
        $('.cc_step_'+number).addClass('active_step');
        $('.cc_step_'+number).show();
        var numberscreens = {
            1: 'wait_screen',
            2: 'play_screen',
            25: 'play_screen',
            3: 'won_screen',
            4: 'play_screen',
            5: 'wait_screen',
        };
        var machine = {
            1: 'cc_machine wait_for_a_game',
            2: 'cc_machine play_game',
            25: 'cc_machine play_game',
            3: 'cc_machine won_game',
            4: 'cc_machine wait_for_a_game',
            5: 'cc_machine wait_for_a_game'
        }
        $('.game_display').attr('class','game_display '+numberscreens[number]);
        $('.cc_machine').attr('class',machine[number]);
        
        if (number == 1 || number == 5) {
            ccgame.fn.clear_animation();
        }
        ccgame.current_screen = number;
    },
    clear_heightlight: function() {
        for(var i = 1; i <= 6; i++) {
            $('.num'+i).removeClass('highlighted');
        }
    },
    clear_animation: function() {
        for (var i = 1; i <= 6; i++) {
            ccgame.fn.clear_element($('.num' + i));
        }
        $('.combo_block').removeClass('won_combo');
        ccgame.fn.clear_heightlight();
    },
    multiplier_handler: function(element,current_value, type='plus') {
        var cv = parseInt(current_value);
        ccgame.fn.tap();
        switch(type) {
            case 'plus':
                if (cv == 100 || cv == 50) {
                    var result = 100;
                }else if (cv  == 10 ) {
                    var result = 50;
                }else if (cv >= 5 && cv < 10) {
                    var result = 10;
                }else {
                    var result = cv + 1;
                }
                var $span = element.prev('span').find('span');
                ccgame.fn.multiplier_handler_result($span, result);
            break;
            case 'minus':
                if (cv == 100) {
                    var result = 50;
                }else if( cv == 50) {
                    var result = 10;
                }else if (cv > 5 && cv <= 10) {
                    var result = 5;
                }else if (cv <= 1) {
                    var result = 1;
                }else {
                    var result = cv - 1;
                }
                var $span = element.next('span').find('span');
                ccgame.fn.multiplier_handler_result($span, result);
                
            break;
            return false;
        }
    },
    multiplier_handler_result: function($element,result) {
        var classes_for_multipliers = ['text-danger','text-warning','text-success']; // индекс это  номера с конца 0 - последний, 1 - предпоследний и тп
        $element.text(result);
        ccgame.current_multiplier = parseInt(result);
        var result_price = Math.round((cc_ticket_price * ccgame.current_multiplier) * 100000) / 100000;
        
        $('#bet_amount').text( result_price + ' BTC');

        ccgame.fn.recalc_bet_price();
        
        for (var i = 0; i < classes_for_multipliers.length; i++) {
            if (ccgame.multipliers.indexOf(result) == ccgame.multipliers.length - (i + 1)) {
                $element.addClass(classes_for_multipliers[i]);
            }else {
                $element.removeClass(classes_for_multipliers[i]);
            }
        }
    },
    animate:function(status) {
        switch(status) {
            case 'wait_for_a_game':
                ccgame.fn.animation_wait();
            break;
            case 'play_game':
                ccgame.fn.animation_play();
            break;
        }
    },
    clear_element: function(selector) {
        for(var i = 0; i <= 7; i++ ) {
            selector.removeClass('digit_'+i);
        }
    },
    animation_bulb_reverse: function(num,stop,iters) {
        // запускает анимацию цифры num, с остановкой на номере stop на итерации iters
        var easeout = 100;
        var step = 14;
        
        for(var i = 1; i <= iters; i++ ) {
            easeout += step;
            var rnd = Math.floor(Math.random() * (8));
            if (i == iters) {
                rnd = stop;
            }
            ccgame.fn.set_digit_value($(document).find('.num'+num),'digit_'+rnd, easeout);
        }
    },
    set_digit_value: function(selector,digit, offset) {
        setTimeout(function() {
            ccgame.fn.clear_element(selector); // очищаем
            selector.addClass(digit); // показываем цифру
        },offset);
    },
    animation_play: function() {
        if ($(document).find('.play_game').length && ccgame.last_combo == '') { // нажали играть рандомно анимируем все 6 склянок
            var time = 100;
            var step = 30;
            for(var i = 0; i <= 7; i++) {
                time += step;
                for (var j = 1; j <= 6; j++) {
                    var rnd = Math.floor(Math.random() * 8); // [0..7] генерируем случайное число для показа
                    // .play_game нужен чтобы при смене экрана анимация останавливалась сама
                    // дополнительное смещение для каждой склянки чтобы не крутились одновременно (time + i * 10)
                    ccgame.fn.set_digit_value($('.play_game').find('.num' + j),'digit_' + rnd, (time + i * 10)); 
                }
            }
            setTimeout(function(){ ccgame.fn.animate('play_game')},300); // запускаем себя еще раз
            
        }else if (ccgame.last_combo != '') { // если появилась комбо для рендеринга запускаем механизм остановки на нужных числах

            var cmb = ccgame.last_combo.split('-'); // получаем цифры комбо
            ccgame.fn.clear_heightlight();
            if (cmb.length == 6) {
                for (var i = 1; i <= 6; i++) {
                    if (typeof(cmb[i-1]) != "undefined") {
                        ccgame.fn.animation_bulb_reverse(i, cmb[i-1], i*30); // для каждой следующей склянки увеличиваем количество итераций
                    }
                }
            }
        }
    },
    animation_wait:function() {
        if ($(document).find('.wait_for_a_game').length) {
            setTimeout(function() {
                if ($(document).find('.wait_for_a_game').length) { // для выхода через удаление класса
                    $('.wait_for_a_game').find('.num1, .num2, .num3, .num4, .num5,.num6').addClass("highlighted");//,1000,'easeOutSine');
                }
            }, 2000);
        }
    }
}

ccgame.init();