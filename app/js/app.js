function Methods() {

    if(typeof window._methods !== 'undefined'){
        return window._methods;
    }

    var Methods = {
        processing_post: false,
        processing_block : true,
        actions : {},
        protection : false,
        preloaded : [],
        ADD_ACTION : function(name, callback){
            Methods.actions[name] = callback;
        },
        CALL_ACTION : function(name, context, data){
            if(name in Methods.actions){
                Methods.actions[name].apply(context, data);
            }
        },
        INIT_EVENTS : function(){
            $(document).on('click', "a[href='#']", function (e) {
                e.preventDefault();
                e.stopPropagation();
            }).on('click', '[data-action]', function (e) {
                e.preventDefault();
                e.stopPropagation();
                Methods.CALL_ACTION($(this).data('action'), this);
            }).on('change', 'input[data-action]', function(){
                Methods.CALL_ACTION($(this).data('action'), this);
            }).on('change', 'select[data-action]', function(){
                Methods.CALL_ACTION($(this).data('action'), this);
            }).on('click', '[data-cp]', function(e){
                e.preventDefault();
                var data = $(this).data();
                if(data.cp){
                    data.message = data.message || 'Copied to clipboard!';
                    var $container = $('<input>');
                    //$('body').append($container);
                    $(this).after($container);
                    $container.val(data.cp).select();
                    document.execCommand("copy");
                    $container.remove();
                    Methods.SHOW_TOASTR(data.message, 'success');
                }
            }).on('keyup', 'input[data-actionkey]',function(){
                Methods.CALL_ACTION($(this).data('actionkey'), this);
            }).on('click touchend', '[data-href]', function(e){
                e.preventDefault();
                window.location.href = $(this).data('href');
            }).on('keyup', 'input[data-submit]', function(e){
                if(e.which == 13) {
                    e.preventDefault();
                    var action = $(this).data('submit');
                    $('[data-action='+action+']').trigger('click');
                }
            });
        },
        SHOW_MODAL : function(content, __callback, _class){
            var $container = $('#autoModals');
            _class = _class || '';
            if($container.length === 0){
                $container = $('<div>').attr('id', 'autoModals');
                $('body').append($container);
            }
            var $modal = $('<div>').addClass('modal in').attr('tabindex', -1).attr('role', 'dialog').attr('aria-labelledby', 'myModalLabel').append($('<div>').addClass('modal-dialog ' + _class).append($('<div>').addClass('modal-content'))).hide();
            $modal.find('.modal-content').append(content);
            $container.empty().append($modal);
            if(__callback){
                $modal.on('hide.bs.modal', function(){
                    __callback.apply(this);
                });
            }
            //Methods.CLOSE_MODAL();
            // $($modal).modal();
        },
        GET_MODAL : function(){
            var $container = $('#autoModals');
            return $container.children('.modal');
        },
        CLOSE_MODAL : function(){
            if ($(document).find('.modal').length) {
                $(document).find('.modal').modal('hide');
            }
            // fix если открыты последовательно несколько модалок
            $('body').children('.modal-backdrop').stop(1,1).fadeOut(250, function(){
                $('body').removeClass('modal-open');
                $(this).remove();
            });
        },
        SCROLL_TO : function(selector, offset, speed) {
            var $element = $(selector);
            var speed = speed || 600;
            if ($element.length) {
                $('html,body').animate({scrollTop: $element.offset().top - offset}, speed);
            }
        },
        preload : function(files){
            $(files).each(function(){
                Methods.preloaded[this] = $('<img/>').attr('src', this);
            });
        }
    };

    Methods.INIT_EVENTS();

    window._methods = Methods;

    return Methods;
    
}(jQuery);