$(function() {

  // Custom JS

  
  var isMobile = window.matchMedia("(max-width: 576px)").matches;
  var isTablet = window.matchMedia("(max-width: 768px)").matches;

  if (!isMobile) {
    $('.side-left').addClass('opacity')
    $('.circle').addClass('opacity')
    $('.side-right').addClass('opacity')
    $('.fall').addClass('opacity')
    $('.upward').addClass('opacity')
  }
  //Animation
  if(!isMobile) {

    var controller = new ScrollMagic.Controller();

  $('.side-left').each(function(){

    var myScene = new ScrollMagic.Scene({
      triggerElement: this,
      duration: '0',
      triggerHook: 0.9

    })
    .setClassToggle(this, 'left')
    // .addIndicators({
    //   name: 'fade scene',
    //   colorTrigger: 'red',
    //   colorStart: 'green',
    //   colorEnd: 'pink'
    // })
    .addTo(controller);
    

  });


  var controller = new ScrollMagic.Controller();

  $('.side-right').each(function(){

    var myScene = new ScrollMagic.Scene({
      triggerElement: this,
      duration: '0',
      triggerHook: 0.9

    })
    .setClassToggle(this, 'right')
    // .addIndicators({
    //   name: 'fade scene',
    //   colorTrigger: 'red',
    //   colorStart: 'green',
    //   colorEnd: 'pink'
    // })
    .addTo(controller);
    

  });


  $('.circle').each(function(){

    var SceneCircle = new ScrollMagic.Scene({
      triggerElement: this,
      duration: '0',
      triggerHook: 0.8

    })
    .setClassToggle(this, 'circle-show')
    // .addIndicators({
    //   name: 'fade scene',
    //   colorTrigger: 'red',
    //   colorStart: 'green',
    //   colorEnd: 'pink'
    // })
    .addTo(controller);
    

  });

      var controller = new ScrollMagic.Controller();

      $('.fall').each(function(){

        var myScene = new ScrollMagic.Scene({
          triggerElement: this,
          duration: '0',
          triggerHook: 0.9

        })
        .setClassToggle(this, 'down')
        // .addIndicators({
        //   name: 'fade scene',
        //   colorTrigger: 'red',
        //   colorStart: 'green',
        //   colorEnd: 'pink'
        // })
        .addTo(controller);

      });

      var controller = new ScrollMagic.Controller();

      $('.upward').each(function(){

        var myScene = new ScrollMagic.Scene({
          triggerElement: this,
          duration: '0',
          triggerHook: 0.9

        })
        .setClassToggle(this, 'up')
        // .addIndicators({
        //   name: 'fade scene',
        //   colorTrigger: 'red',
        //   colorStart: 'green',
        //   colorEnd: 'pink'
        // })
        .addTo(controller);

      });

      var showNotifications = false;

      var sectionScene = new ScrollMagic.Scene ({
        triggerElement: '.flips-star',
        duration:'700',
        triggerHook: 0.7
      })
     /* .addIndicators({
        name: 'notifications',
        color: 'black',
        colorStart: 'yellow',
        colorEnd: 'blue'
      })*/
      .addTo(controller);


      // sectionScene.on("enter", function (event) {
      //   showNotifications = true
      // });

      // sectionScene.on("leave", function (event) {
      //   showNotifications = false
      // });

  }
  
      // if (!isMobile) {
      //   var timerNotifications = setInterval(function(){
      //     if (showNotifications) {
      //       $.growl({ 
      //         title: "Growl", 
      //         message: "The kitten is awake!",
      //         delayOnHover: false,
      //         duration: 4500,
      //         fixed: false,
      //         size: 'large'
      //       });

      //     } else {
      //       $('.growl').remove();
      //     }
          
      //   },1000);
      // }

  if(!isTablet) {
    var timerNotifications = setInterval (function(){
      new Noty({
        type: 'success',
        layout: 'topRight',
        text: '<div class = "notify"><p class="notify-title">2.00 btc gets al****</p><p class="notify-message">draw #212, combo 3 4 43 18 7</p></div>',
        container: '#noty_layout__topRight',
        timeout: '5000',
        visibilityControl: false
      }).show();
      Noty.setMaxVisible(3);
    }, 4000)
  }
  
  // if(!isMobile) {
  //   $('#fullpage').fullpage({
  //     //modalOptions here
  //     autoScrolling: true,
  //     scrollHorizontally: true,
  //     scrollingSpeed: 500,
  //     fitToSection: 'true',

  //     afterLoad: function(section, origin, destination, direction){
  //       if(origin && origin.anchor === 'header') {
  //         $('.fall').addClass('down');
  //         //$('.circle').addClass('circle-show');
  //       }
  //       if(origin && origin.anchor === 'flips-star') {
  //         $('.side-left').addClass('left');
  //         //$('.circle').addClass('circle-show');
  //       }
  //       if(origin && origin.anchor === 'rapid') {
  //         $('.side-right').addClass('right');
  //         //$('.circle').addClass('circle-show');
  //       }
  //       if(origin && origin.anchor === 'chain-code') {
  //         $('.side-left').addClass('left');
  //        // $('.circle').addClass('circle-show');
  //       }
  //       if(origin && origin.anchor === 'pirate-bay') {
  //         $('.side-right').addClass('right');
  //         //$('.circle').addClass('circle-show');
  //       }
  //       if(origin && origin.anchor === 'footer') {
  //         $('.upward').addClass('up');
  //         //$('.circle').addClass('circle-show');
  //       }
  //     },
  //     onLeave: function(origin, destination, direction) {
  //       console.log('onLeave', origin);
  //       if(origin && origin.anchor === 'header') {
  //         $('.fall').removeClass('down');
  //         //$('.circle').removeClass('circle-show');
  //       }
  //       if(origin && origin.anchor === 'flips-star') {
  //         $('.side-left').removeClass('left');
  //        // $('.circle').removeClass('circle-show');
  //       }
  //       if(origin && origin.anchor === 'rapid') {
  //         $('.side-right').removeClass('right');
  //         //$('.circle').removeClass('circle-show');
  //       }
  //       if(origin && origin.anchor === 'chain-code') {
  //         $('.side-left').removeClass('left');
  //         //$('.circle').removeClass('circle-show');
  //       }
  //       if(origin && origin.anchor === 'pirate-bay') {
  //         $('.side-right').removeClass('right');
  //         //$('.circle').removeClass('circle-show');
  //       }
  //       if(origin && origin.anchor === 'footer') {
  //         $('.upward').removeClass('up');
  //         //$('.circle').removeClass('circle-show');
  //       }
  //     }
  //   });

  //   //methods
  //   $.fn.fullpage.setAllowScrolling(true);
  //   fullpage_api.setFitToSection(true);
  // }

  var modalOptions = {
      removalDelay: 500,
      callbacks: {
          beforeOpen: function() {
              $('.modal-custom-overlay').addClass('modal-custom-overlay-open');
              this.st.mainClass = 'mfp-zoom-in';
          },
          afterClose: function () {
              $('.modal-custom-overlay').removeClass('modal-custom-overlay-open');
          }
      }
  };
  $("#modal").magnificPopup(modalOptions);

  $("#mobile-modal").magnificPopup(modalOptions);

  $('#chain-game').magnificPopup(modalOptions);

  $('#pirate-play').magnificPopup({
      removalDelay: 500,
      callbacks: {
          beforeOpen: function() {
              $('.modal-custom-overlay').addClass('modal-custom-overlay-open');
              this.st.mainClass = 'mfp-zoom-in';
          },
          afterClose: function () {
              $('.modal-custom-overlay').removeClass('modal-custom-overlay-open');
              stopPirateBayBgMusic();
          },
          open: function () {
              setTimeout(function () {
                  $('#playGame').click();
              }, 500)
          }
      }
  });

  // $('#pirate-play').animatedModal({
  //   modalTarget: 'pirate-modal',
  //   overflow: 'scroll',
  //   beforeOpen: function() {
  //     //fullpage_api.silentMoveTo('header')
  //   },
  //   afterOpen: function() {
  //     //fullpage_api.setAllowScrolling(false, 'down'),
  //     setTimeout(function () {
  //       $('#playGame').click();
  //       // PirateBayIntro.start();
  //     }, 500);
  //   },
  //   afterClose: function() {
  //     //fullpage_api.setAllowScrolling(true, 'down'),
  //     stopPirateBayBgMusic()
  //     //fullpage_api.silentMoveTo('pirate-bay')
  //   }
  // });
  

  $('#registration-form').submit(function(e) {
    e.preventDefault();
    $(this).fadeOut(300);
    $('.type-email').fadeOut(300);
    $('.protected').fadeOut(300, function() {
      $('.thanks').fadeIn(300);
    });
  });
  $('.refresh-reg').click(function() {
    $('.thanks').fadeOut(300, function(){
      $('#registration-form').fadeIn(300);
      $('.type-email').fadeIn(300);
      $('.protected').fadeIn(300);
    });
  });

  $('#registration-flip').submit(function(e) {
    e.preventDefault();
    $(this).fadeOut(300, function(){
      $('.thanks-flip').fadeIn(300);
    });
  });

  $('.refresh-reg-flip').click(function(){
    $('.thanks-flip').fadeOut(300, function(){
      $('#registration-flip').fadeIn(300);
    });
  });

  $('#registration-rapid').submit(function(e) {
    e.preventDefault();
    $('.jackpot').fadeOut(300);
    $('.ticket').fadeOut(300);
    $(this).fadeOut(300, function(){
      $('.thanks-rapid').fadeIn(300);
    });
  });

  $('.refresh-reg-rapid').click(function(){
    $('.thanks-rapid').fadeOut(300, function(){
      $('#registration-rapid').fadeIn(300);
      $('.jackpot').fadeIn(300);
      $('.ticket').fadeIn(300);
    });
  });

  $('#registration-footer').submit(function(e) {
    e.preventDefault();
    $('.drawing').fadeOut(300);
    $(this).fadeOut(300, function(){
      $('.thanks-footer').fadeIn(300);
    });
  });

  $('.refresh-reg-footer').click(function(){
    $('.thanks-footer').fadeOut(300, function(){
      $('#registration-footer').fadeIn(300);
      $('.drawing').fadeIn(300);
    });
  });

  
  $('.singup-modal').click(function(){
    $('.modal-top').fadeOut(300);
    $('.video').fadeOut(300, function(){
      $('.reg-modal').fadeIn(300);
    });
  });

  $('#registration-modal').submit(function(e) {
    e.preventDefault();
    $('.input-group-modal').fadeOut(300, function(){
      $('.thanks-modal').fadeIn(300);
    });
  });

  $('#registration-mobile').submit(function(e) {
    e.preventDefault();
    $('.input-group-mobile').fadeOut(300, function(){
      $('.thanks-mobile').fadeIn(300);
    });
  });
  
    
  document.querySelectorAll( "input" ).forEach(function(value) {
    var el = value;
    el.addEventListener( "invalid",
      function( event ) {
          event.preventDefault();
          value.focus();
      });
    });
  });

  // if(!isMobile) {
  //   $('#fullpage').fullpage({
  //     //options here
  //     autoScrolling:true,
  //     scrollHorizontally: true,

  //     afterLoad: function(section, origin, destination, direction){
  //       if(origin && origin.anchor === 'flips-star') {
  //         $('.side-left').addClass('left');
  //       }
  //     },
  //     onLeave: function(origin, destination, direction) {
  //       console.log('onLeave', origin);
  //       if(origin && origin.anchor === 'flips-star') {
  //         $('.side-left').removeClass('left');
  //       }
  //     }
  //   });

  //   //methods
  //   $.fn.fullpage.setAllowScrolling(true);
  //   fullpage_api.setFitToSection(true);

  // }

  $(window).on('load', function(){
    $('.preloader').fadeOut('slow');
  })
  